package com.gitlab.filmica;

import com.gitlab.filmica.menu.MainMenu;

public class Main {

    public static void main(String[] args) {
        MainMenu.run();
    }
}
